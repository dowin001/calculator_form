﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace Shapes_Form
{
    public partial class Form2 : Form
    {
        int decimals = 10;
        int history = 10;
        string conString = ConfigurationManager.ConnectionStrings["Shapes_Form.Properties.Settings.Calculator_HistoryConnectionString"].ConnectionString;

        public Form2()
        {
            InitializeComponent();
            getFromDatabase();
            decimalNumber.Value = decimals;
            historyNumber.Value = history;
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            decimals = Convert.ToInt32(decimalNumber.Value);
            history = Convert.ToInt32(historyNumber.Value);

            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = "UPDATE Calculator_Variables SET decimalPlaces = @decimals, historyEntries = @history;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.Add("@decimals", SqlDbType.Int).Value = decimals;
            cmd.Parameters.Add("@history", SqlDbType.Int).Value = history;
            cmd.ExecuteNonQuery();

            this.Close();
        }

        private void getFromDatabase()
        {
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = "SELECT * FROM Calculator_Variables";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                decimals = Convert.ToInt32(reader["decimalPlaces"]);
                history = Convert.ToInt32(reader["historyEntries"]);
            }

            con.Close();
        }
    }
}
