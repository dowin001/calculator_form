﻿
namespace Shapes_Form
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.circleRadio = new System.Windows.Forms.RadioButton();
            this.triangleRadio = new System.Windows.Forms.RadioButton();
            this.rectangleRadio = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.radiusTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.rectLabel2 = new System.Windows.Forms.Label();
            this.rectLabel1 = new System.Windows.Forms.Label();
            this.rectTextBox2 = new System.Windows.Forms.TextBox();
            this.rectTextBox1 = new System.Windows.Forms.TextBox();
            this.triLabel3 = new System.Windows.Forms.Label();
            this.triLabel2 = new System.Windows.Forms.Label();
            this.triLabel1 = new System.Windows.Forms.Label();
            this.triTextBox1 = new System.Windows.Forms.TextBox();
            this.triTextBox3 = new System.Windows.Forms.TextBox();
            this.triTextBox2 = new System.Windows.Forms.TextBox();
            this.rectLabel3 = new System.Windows.Forms.Label();
            this.rectTextBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.answerTextBox = new System.Windows.Forms.TextBox();
            this.optionsButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Perimeter",
            "Area",
            "Surface Area",
            "Volume"});
            this.comboBox1.Location = new System.Drawing.Point(307, 46);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.selection_change);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(304, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select an operation to calculate:";
            // 
            // circleRadio
            // 
            this.circleRadio.AutoSize = true;
            this.circleRadio.Location = new System.Drawing.Point(24, 92);
            this.circleRadio.Name = "circleRadio";
            this.circleRadio.Size = new System.Drawing.Size(51, 17);
            this.circleRadio.TabIndex = 2;
            this.circleRadio.TabStop = true;
            this.circleRadio.Text = "Circle";
            this.circleRadio.UseVisualStyleBackColor = true;
            this.circleRadio.CheckedChanged += new System.EventHandler(this.circleRadio_CheckedChanged);
            // 
            // triangleRadio
            // 
            this.triangleRadio.AutoSize = true;
            this.triangleRadio.Location = new System.Drawing.Point(554, 92);
            this.triangleRadio.Name = "triangleRadio";
            this.triangleRadio.Size = new System.Drawing.Size(63, 17);
            this.triangleRadio.TabIndex = 3;
            this.triangleRadio.TabStop = true;
            this.triangleRadio.Text = "Triangle";
            this.triangleRadio.UseVisualStyleBackColor = true;
            this.triangleRadio.CheckedChanged += new System.EventHandler(this.triangleRadio_CheckedChanged);
            // 
            // rectangleRadio
            // 
            this.rectangleRadio.AutoSize = true;
            this.rectangleRadio.Location = new System.Drawing.Point(302, 92);
            this.rectangleRadio.Name = "rectangleRadio";
            this.rectangleRadio.Size = new System.Drawing.Size(74, 17);
            this.rectangleRadio.TabIndex = 4;
            this.rectangleRadio.TabStop = true;
            this.rectangleRadio.Text = "Rectangle";
            this.rectangleRadio.UseVisualStyleBackColor = true;
            this.rectangleRadio.CheckedChanged += new System.EventHandler(this.rectangleRadio_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Radius:";
            // 
            // radiusTextBox
            // 
            this.radiusTextBox.Location = new System.Drawing.Point(89, 127);
            this.radiusTextBox.Name = "radiusTextBox";
            this.radiusTextBox.Size = new System.Drawing.Size(100, 20);
            this.radiusTextBox.TabIndex = 6;
            this.radiusTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radiusTextBox_MouseClick);
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(349, 264);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 7;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // rectLabel2
            // 
            this.rectLabel2.AutoSize = true;
            this.rectLabel2.Location = new System.Drawing.Point(318, 161);
            this.rectLabel2.Name = "rectLabel2";
            this.rectLabel2.Size = new System.Drawing.Size(41, 13);
            this.rectLabel2.TabIndex = 8;
            this.rectLabel2.Text = "Height:";
            // 
            // rectLabel1
            // 
            this.rectLabel1.AutoSize = true;
            this.rectLabel1.Location = new System.Drawing.Point(318, 127);
            this.rectLabel1.Name = "rectLabel1";
            this.rectLabel1.Size = new System.Drawing.Size(43, 13);
            this.rectLabel1.TabIndex = 9;
            this.rectLabel1.Text = "Length:";
            // 
            // rectTextBox2
            // 
            this.rectTextBox2.Location = new System.Drawing.Point(367, 158);
            this.rectTextBox2.Name = "rectTextBox2";
            this.rectTextBox2.Size = new System.Drawing.Size(100, 20);
            this.rectTextBox2.TabIndex = 10;
            this.rectTextBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rectTextBox2_MouseClick);
            // 
            // rectTextBox1
            // 
            this.rectTextBox1.Location = new System.Drawing.Point(367, 124);
            this.rectTextBox1.Name = "rectTextBox1";
            this.rectTextBox1.Size = new System.Drawing.Size(100, 20);
            this.rectTextBox1.TabIndex = 11;
            this.rectTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rectTextBox1_MouseClick);
            // 
            // triLabel3
            // 
            this.triLabel3.AutoSize = true;
            this.triLabel3.Location = new System.Drawing.Point(551, 194);
            this.triLabel3.Name = "triLabel3";
            this.triLabel3.Size = new System.Drawing.Size(40, 13);
            this.triLabel3.TabIndex = 12;
            this.triLabel3.Text = "Side 3:";
            // 
            // triLabel2
            // 
            this.triLabel2.AutoSize = true;
            this.triLabel2.Location = new System.Drawing.Point(551, 161);
            this.triLabel2.Name = "triLabel2";
            this.triLabel2.Size = new System.Drawing.Size(40, 13);
            this.triLabel2.TabIndex = 13;
            this.triLabel2.Text = "Side 2:";
            // 
            // triLabel1
            // 
            this.triLabel1.AutoSize = true;
            this.triLabel1.Location = new System.Drawing.Point(551, 124);
            this.triLabel1.Name = "triLabel1";
            this.triLabel1.Size = new System.Drawing.Size(40, 13);
            this.triLabel1.TabIndex = 14;
            this.triLabel1.Text = "Side 1:";
            // 
            // triTextBox1
            // 
            this.triTextBox1.Location = new System.Drawing.Point(600, 121);
            this.triTextBox1.Name = "triTextBox1";
            this.triTextBox1.Size = new System.Drawing.Size(100, 20);
            this.triTextBox1.TabIndex = 15;
            this.triTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.triTextBox1_MouseClick);
            // 
            // triTextBox3
            // 
            this.triTextBox3.Location = new System.Drawing.Point(600, 191);
            this.triTextBox3.Name = "triTextBox3";
            this.triTextBox3.Size = new System.Drawing.Size(100, 20);
            this.triTextBox3.TabIndex = 16;
            this.triTextBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.triTextBox3_MouseClick);
            // 
            // triTextBox2
            // 
            this.triTextBox2.Location = new System.Drawing.Point(600, 158);
            this.triTextBox2.Name = "triTextBox2";
            this.triTextBox2.Size = new System.Drawing.Size(100, 20);
            this.triTextBox2.TabIndex = 17;
            this.triTextBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.triTextBox2_MouseClick);
            // 
            // rectLabel3
            // 
            this.rectLabel3.AutoSize = true;
            this.rectLabel3.Location = new System.Drawing.Point(318, 194);
            this.rectLabel3.Name = "rectLabel3";
            this.rectLabel3.Size = new System.Drawing.Size(34, 13);
            this.rectLabel3.TabIndex = 18;
            this.rectLabel3.Text = "Extra:";
            // 
            // rectTextBox3
            // 
            this.rectTextBox3.Location = new System.Drawing.Point(367, 191);
            this.rectTextBox3.Name = "rectTextBox3";
            this.rectTextBox3.Size = new System.Drawing.Size(100, 20);
            this.rectTextBox3.TabIndex = 19;
            this.rectTextBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rectTextBox3_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(294, 309);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Result:";
            // 
            // answerTextBox
            // 
            this.answerTextBox.Location = new System.Drawing.Point(340, 306);
            this.answerTextBox.Name = "answerTextBox";
            this.answerTextBox.Size = new System.Drawing.Size(127, 20);
            this.answerTextBox.TabIndex = 21;
            // 
            // optionsButton
            // 
            this.optionsButton.Location = new System.Drawing.Point(713, 9);
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.Size = new System.Drawing.Size(75, 23);
            this.optionsButton.TabIndex = 22;
            this.optionsButton.Text = "Options";
            this.optionsButton.UseVisualStyleBackColor = true;
            this.optionsButton.Click += new System.EventHandler(this.optionsButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(713, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "History";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 338);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.optionsButton);
            this.Controls.Add(this.answerTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rectTextBox3);
            this.Controls.Add(this.rectLabel3);
            this.Controls.Add(this.triTextBox2);
            this.Controls.Add(this.triTextBox3);
            this.Controls.Add(this.triTextBox1);
            this.Controls.Add(this.triLabel1);
            this.Controls.Add(this.triLabel2);
            this.Controls.Add(this.triLabel3);
            this.Controls.Add(this.rectTextBox1);
            this.Controls.Add(this.rectTextBox2);
            this.Controls.Add(this.rectLabel1);
            this.Controls.Add(this.rectLabel2);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.radiusTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rectangleRadio);
            this.Controls.Add(this.triangleRadio);
            this.Controls.Add(this.circleRadio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton circleRadio;
        private System.Windows.Forms.RadioButton triangleRadio;
        private System.Windows.Forms.RadioButton rectangleRadio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox radiusTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label rectLabel2;
        private System.Windows.Forms.Label rectLabel1;
        private System.Windows.Forms.TextBox rectTextBox2;
        private System.Windows.Forms.TextBox rectTextBox1;
        private System.Windows.Forms.Label triLabel3;
        private System.Windows.Forms.Label triLabel2;
        private System.Windows.Forms.Label triLabel1;
        private System.Windows.Forms.TextBox triTextBox1;
        private System.Windows.Forms.TextBox triTextBox3;
        private System.Windows.Forms.TextBox triTextBox2;
        private System.Windows.Forms.Label rectLabel3;
        private System.Windows.Forms.TextBox rectTextBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox answerTextBox;
        private System.Windows.Forms.Button optionsButton;
        private System.Windows.Forms.Button button1;
    }
}

