﻿
namespace Shapes_Form
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.decimalNumber = new System.Windows.Forms.NumericUpDown();
            this.historyNumber = new System.Windows.Forms.NumericUpDown();
            this.acceptButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.decimalNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.historyNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of decimals in result:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of entries in calculation history:";
            // 
            // decimalNumber
            // 
            this.decimalNumber.Location = new System.Drawing.Point(209, 38);
            this.decimalNumber.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.decimalNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.decimalNumber.Name = "decimalNumber";
            this.decimalNumber.Size = new System.Drawing.Size(50, 20);
            this.decimalNumber.TabIndex = 2;
            this.decimalNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // historyNumber
            // 
            this.historyNumber.Location = new System.Drawing.Point(209, 75);
            this.historyNumber.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.historyNumber.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.historyNumber.Name = "historyNumber";
            this.historyNumber.Size = new System.Drawing.Size(50, 20);
            this.historyNumber.TabIndex = 3;
            this.historyNumber.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(115, 108);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 4;
            this.acceptButton.Text = "Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 143);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.historyNumber);
            this.Controls.Add(this.decimalNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.decimalNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.historyNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown decimalNumber;
        private System.Windows.Forms.NumericUpDown historyNumber;
        private System.Windows.Forms.Button acceptButton;
    }
}