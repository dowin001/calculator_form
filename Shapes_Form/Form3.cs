﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace Shapes_Form
{
    public partial class Form3 : Form
    {

        int history = 0;
        string conString = ConfigurationManager.ConnectionStrings["Shapes_Form.Properties.Settings.Calculator_HistoryConnectionString"].ConnectionString;

        public Form3()
        {
            InitializeComponent();
            getFromDatabase();
            getHistory();

        }

        private void getFromDatabase()
        {
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = "SELECT * FROM Calculator_Variables";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                history = Convert.ToInt32(reader["historyEntries"]);
            }

            con.Close();
        }

        private void getHistory()
        {
            int count = 0;
            
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = "SELECT * FROM CalculationHistory ORDER BY calcTime DESC";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read() && count < history)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                row.Cells[0].Value = getRow(reader);
                dataGridView1.Rows.Add(row);

                count++;
            }
        }

        private string getRow(SqlDataReader reader)
        {
            string result;

            int num = Convert.ToInt32(reader["numParameters"]);

            if (num == 1)
                result = getRow1(reader);
            else if (num == 2)
                result = getRow2(reader);
            else
                result = getRow3(reader);

            return result;
        }

        private string getRow1(SqlDataReader reader)
        {
            string shape = Convert.ToString(reader["shape"]);
            string operation = Convert.ToString(reader["operation"]);
            string title = Convert.ToString(reader["title1"]);
            string parameter = Convert.ToString(reader["param1"]);
            string answer = Convert.ToString(reader["result"]);

            string result = operation + " of a " + shape + " with a " + title + " of " + parameter + ": " + answer;

            return result;
        }

        private string getRow2(SqlDataReader reader)
        {
            string shape = Convert.ToString(reader["shape"]);
            string operation = Convert.ToString(reader["operation"]);
            string title1 = Convert.ToString(reader["title1"]);
            string parameter1 = Convert.ToString(reader["param1"]);
            string title2 = Convert.ToString(reader["title2"]);
            string parameter2 = Convert.ToString(reader["param2"]);
            string answer = Convert.ToString(reader["result"]);

            string result = operation + " of a " + shape + " with a " + title1 + " of " + parameter1 + " and a " + title2 + " of " + parameter2 + ": " + answer;

            return result;
        }

        private string getRow3(SqlDataReader reader)
        {
            string shape = Convert.ToString(reader["shape"]);
            string operation = Convert.ToString(reader["operation"]);
            string title1 = Convert.ToString(reader["title1"]);
            string parameter1 = Convert.ToString(reader["param1"]);
            string title2 = Convert.ToString(reader["title2"]);
            string parameter2 = Convert.ToString(reader["param2"]);
            string title3 = Convert.ToString(reader["title3"]);
            string parameter3 = Convert.ToString(reader["param3"]);
            string answer = Convert.ToString(reader["result"]);

            string result = operation + " of a " + shape + " with a " + title1 + " of " + parameter1 + ", a " + title2 + " of " + parameter2 + 
                            " and a " + title3 + " of " + parameter3 + ": " + answer;

            return result;
        }
    }
}
