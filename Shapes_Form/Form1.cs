﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace Shapes_Form
{
    public partial class Form1 : Form
    {

        int decimals = 5;
        int history = 5;
        string conString = ConfigurationManager.ConnectionStrings["Shapes_Form.Properties.Settings.Calculator_HistoryConnectionString"].ConnectionString;
        public Form1()
        {
            InitializeComponent();
            comboBox1.Text = "Perimeter";
            rectLabel3.Visible = false;
            rectTextBox3.Visible = false;
        }

        private void selection_change(object sender, EventArgs e)
        {
            if(comboBox1.Text == "Perimeter")
            {
                //Triangle: side, side, side
                //Rectangle: side, side
                //Circle: radius

                circleRadio.Text = "Circle";
                rectangleRadio.Text = "Rectangle";
                triangleRadio.Text = "Triangle";

                rectLabel1.Text = "Length:";
                rectLabel2.Text = "Height:";
                rectLabel3.Visible = false;
                rectTextBox3.Visible = false;

                triangleRadio.Visible = true;
                triLabel1.Visible = true;
                triTextBox1.Visible = true;
                triLabel1.Text = "Side 1:";
                triLabel2.Visible = true;
                triTextBox2.Visible = true;
                triLabel2.Text = "Side 2:";
                triLabel3.Visible = true;
                triTextBox3.Visible = true;
                triLabel3.Text = "Side 3:";

                resetRadio();
                resetTextBoxes();
            }

            if (comboBox1.Text == "Area")
            {
                //Triangle: base, height
                //Rectangle: side, side
                //Circle: radius

                circleRadio.Text = "Circle";
                rectangleRadio.Text = "Rectangle";
                triangleRadio.Text = "Triangle";

                rectLabel1.Text = "Length:";
                rectLabel2.Text = "Height:";
                rectLabel3.Visible = false;
                rectTextBox3.Visible = false;

                triangleRadio.Visible = true;
                triLabel1.Visible = true;
                triTextBox1.Visible = true;
                triLabel1.Text = "Base:";
                triLabel2.Visible = true;
                triTextBox2.Visible = true;
                triLabel2.Text = "Height:";
                triLabel3.Visible = false;
                triTextBox3.Visible = false;

                resetRadio();
                resetTextBoxes();
            }

            if (comboBox1.Text == "Surface Area")
            {
                //Rectangle: side, side, side
                //Sphere: radius

                circleRadio.Text = "Sphere";
                rectangleRadio.Text = "Rectangular Prism";
                triangleRadio.Visible = false;

                rectLabel1.Text = "Length:";
                rectLabel2.Text = "Width:";
                rectLabel3.Visible = true;
                rectTextBox3.Visible = true;
                rectLabel3.Text = "Height:";

                triLabel1.Visible = false;
                triTextBox1.Visible = false;
                triLabel2.Visible = false;
                triTextBox2.Visible = false;
                triLabel3.Visible = false;
                triTextBox3.Visible = false;

                resetRadio();
                resetTextBoxes();
            }

            if (comboBox1.Text == "Volume")
            {
                //Rectangle: side, side, side
                //Sphere: radius

                circleRadio.Text = "Sphere";
                rectangleRadio.Text = "Rectangular Prism";
                triangleRadio.Visible = false;

                rectLabel1.Text = "Length:";
                rectLabel2.Text = "Width:";
                rectLabel3.Visible = true;
                rectTextBox3.Visible = true;
                rectLabel3.Text = "Height:";

                triLabel1.Visible = false;
                triTextBox1.Visible = false;
                triLabel2.Visible = false;
                triTextBox2.Visible = false;
                triLabel3.Visible = false;
                triTextBox3.Visible = false;

                resetRadio();
                resetTextBoxes();
            }
        }

        private void circleRadio_CheckedChanged(object sender, EventArgs e)
        {
            changeRectangleEnabled(false);
            changeTriangleEnabled(false);

            radiusTextBox.Enabled = true;
        }

        private void rectangleRadio_CheckedChanged(object sender, EventArgs e)
        {
            radiusTextBox.Enabled = false;
            changeTriangleEnabled(false);

            changeRectangleEnabled(true);
        }

        private void triangleRadio_CheckedChanged(object sender, EventArgs e)
        {
            radiusTextBox.Enabled = false;
            changeRectangleEnabled(false);

            changeTriangleEnabled(true);
        }

        private void resetRadio()
        {
            circleRadio.Checked = false;
            rectangleRadio.Checked = false;
            triangleRadio.Checked = false;

            radiusTextBox.Enabled = true;
            rectTextBox1.Enabled = true;
            rectTextBox2.Enabled = true;
            rectTextBox3.Enabled = true;
            triTextBox1.Enabled = true;
            triTextBox2.Enabled = true;
            triTextBox3.Enabled = true;
        }

        private void resetTextBoxes()
        {
            radiusTextBox.Text = null;
            rectTextBox1.Text = null;
            rectTextBox2.Text = null;
            rectTextBox3.Text = null;
            triTextBox1.Text = null;
            triTextBox2.Text = null;
            triTextBox3.Text = null;
        }

        private void changeRectangleEnabled(bool b)
        {
            rectTextBox1.Enabled = b;
            rectTextBox2.Enabled = b;
            rectTextBox3.Enabled = b;
        }

        private void changeTriangleEnabled(bool b)
        {
            triTextBox1.Enabled = b;
            triTextBox2.Enabled = b;
            triTextBox3.Enabled = b;
        }

        private void radiusTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            circleRadio.Checked = true;
        }

        private void rectTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            rectangleRadio.Checked = true;
        }

        private void rectTextBox2_MouseClick(object sender, MouseEventArgs e)
        {
            rectangleRadio.Checked = true;
        }

        private void rectTextBox3_MouseClick(object sender, MouseEventArgs e)
        {
            rectangleRadio.Checked = true;
        }

        private void triTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            triangleRadio.Checked = true;
        }

        private void triTextBox2_MouseClick(object sender, MouseEventArgs e)
        {
            triangleRadio.Checked = true;
        }

        private void triTextBox3_MouseClick(object sender, MouseEventArgs e)
        {
            triangleRadio.Checked = true;
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            if (!circleRadio.Checked && !rectangleRadio.Checked && !triangleRadio.Checked)
            {
                MessageBox.Show("Please select a shape.");
                return;
            }

            getFromDatabase();
            
            if (comboBox1.Text == "Perimeter")
                calculatePerimeter();
            else if (comboBox1.Text == "Area")
                calculateArea();
            else if (comboBox1.Text == "Surface Area")
                calculateSurfaceArea();
            else
                calculateVolume();
        }

        private void calculatePerimeter()
        {
            if (circleRadio.Checked)
            {
                bool valid = checkErrorOne();

                if (!valid)
                    showErrorText();
                else
                {
                    double radius = double.Parse(radiusTextBox.Text);
                    double result = (2 * Math.PI * radius);
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, result, calcTime) " +
                                   "VALUES ('Circle', 'Perimeter', '1', 'Radius', '" + radius + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
            else if (rectangleRadio.Checked)
            {
                bool valid = checkErrorTwo();

                if (!valid)
                    showErrorText();
                else
                {
                    double a = double.Parse(rectTextBox1.Text);
                    double b = double.Parse(rectTextBox2.Text);
                    double result = (2 * a) + (2 * b);
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, title2, param2, result, calcTime) " +
                                   "VALUES ('Rectangle', 'Perimeter', '2', 'Length', '" + a + "', 'Height', '" + b + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
            else
            {
                bool valid = checkErrorThree();

                if (!valid)
                    showErrorText();
                else
                {
                    double a = double.Parse(triTextBox1.Text);
                    double b = double.Parse(triTextBox2.Text);
                    double c = double.Parse(triTextBox3.Text);
                    double result = a + b + c;
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, title2, param2, title3, param3, result, calcTime) " +
                                   "VALUES ('Triangle', 'Perimeter', '3', 'Side', '" + a + "', 'Side', '" + b + "', 'Side', '" + c + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
        }

        private void calculateArea()
        {
            if (circleRadio.Checked)
            {
                bool valid = checkErrorOne();

                if (!valid)
                    showErrorText();
                else
                {
                    double radius = double.Parse(radiusTextBox.Text);
                    double result = Math.PI * (radius * radius);
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, result, calcTime) " +
                                   "VALUES ('Circle', 'Area', '1', 'Radius', '" + radius + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
            else if (rectangleRadio.Checked)
            {
                bool valid = checkErrorTwo();

                if (!valid)
                    showErrorText();
                else
                {
                    double a = double.Parse(rectTextBox1.Text);
                    double b = double.Parse(rectTextBox2.Text);
                    double result = a * b;
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, title2, param2, result, calcTime) " +
                                   "VALUES ('Rectangle', 'Area', '2', 'Length', '" + a + "', 'Height', '" + b + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
            else
            {
                bool valid = checkErrorTwo();

                if (!valid)
                    showErrorText();
                else
                {
                    double b = double.Parse(triTextBox1.Text);
                    double h = double.Parse(triTextBox2.Text);
                    double result = (b * h) / 2;
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, title2, param2, result, calcTime) " +
                                   "VALUES ('Triangle', 'Area', '2', 'Base', '" + b + "', 'Height', '" + h + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
        }

        private void calculateSurfaceArea()
        {
            if (circleRadio.Checked)
            {
                bool valid = checkErrorOne();

                if (!valid)
                    showErrorText();
                else
                {
                    double radius = double.Parse(radiusTextBox.Text);
                    double result = 4 * Math.PI * (radius * radius);
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, result, calcTime) " +
                                   "VALUES ('Sphere', 'Surface Area', '1', 'Radius', '" + radius + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }

            else
            {
                bool valid = checkErrorThree();

                if (!valid)
                    showErrorText();
                else
                {
                    double a = double.Parse(rectTextBox1.Text);
                    double b = double.Parse(rectTextBox2.Text);
                    double c = double.Parse(rectTextBox3.Text);
                    double result = (2 * a * b) + (2 * b * c) + (2 * a * c);
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, title2, param2, title3, param3, result, calcTime) " +
                                   "VALUES ('Rectangular Prism', 'Surface Area', '3', 'Length', '" + a + "', 'Width', '" + b + "', 'Height', '" + c + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
        }

        private void calculateVolume()
        {
            if (circleRadio.Checked)
            {
                bool valid = checkErrorOne();

                if (!valid)
                    showErrorText();
                else
                {
                    double radius = double.Parse(radiusTextBox.Text);
                    double result = (4 * Math.PI * (radius * radius * radius)) / 3;
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, result, calcTime) " +
                                   "VALUES ('Sphere', 'Volume', '1', 'Radius', '" + radius + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
            else
            {
                bool valid = checkErrorThree();

                if (!valid)
                    showErrorText();
                else
                {
                    double a = double.Parse(rectTextBox1.Text);
                    double b = double.Parse(rectTextBox2.Text);
                    double c = double.Parse(rectTextBox3.Text);
                    double result = a * b * c;
                    result = Math.Round(result, decimals);
                    answerTextBox.Text = result.ToString();
                    DateTime calcTime = DateTime.Now;
                    string query = "INSERT INTO CalculationHistory (shape, operation, numParameters, title1, param1, title2, param2, title3, param3, result, calcTime) " +
                                   "VALUES ('Rectangular Prism', 'Volume', '3', 'Length', '" + a + "', 'Width', '" + b + "', 'Height', '" + c + "', '" + result + "', '" + calcTime + "');";
                    insertData(query);
                }
            }
        }

        private bool checkErrorOne()
        {
            if (radiusTextBox.Text == null)
                return false;


            if (double.TryParse(radiusTextBox.Text, out double radius))
            {
                if (radius <= 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        private bool checkErrorTwo()
        {
            if (rectangleRadio.Checked)
            {
                if (rectTextBox1.Text == null || rectTextBox2.Text == null)
                    return false;

                if (double.TryParse(rectTextBox1.Text, out double rect1)
                    && double.TryParse(rectTextBox2.Text, out double rect2))
                {
                    if (rect1 <= 0 || rect2 <= 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
            else
            {
                if (triTextBox1.Text == null || triTextBox2.Text == null)
                    return false;

                if (double.TryParse(triTextBox1.Text, out double tri1)
                    && double.TryParse(triTextBox2.Text, out double tri2))
                {
                    if (tri1 <= 0 || tri2 <= 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
        }
        
        private bool checkErrorThree()
        {
            if (rectangleRadio.Checked)
            {
                if (rectTextBox1.Text == null || rectTextBox2.Text == null || rectTextBox3.Text == null)
                    return false;

                if (double.TryParse(rectTextBox1.Text, out double rect1)
                    && double.TryParse(rectTextBox2.Text, out double rect2)
                    && double.TryParse(rectTextBox3.Text, out double rect3))
                {
                    if (rect1 <= 0 || rect2 <= 0 || rect3 <= 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
            else
            {
                if (triTextBox1.Text == null || triTextBox2.Text == null || triTextBox3.Text == null)
                    return false;

                if (double.TryParse(triTextBox1.Text, out double tri1)
                    && double.TryParse(triTextBox2.Text, out double tri2)
                    && double.TryParse(triTextBox3.Text, out double tri3))
                {
                    if (tri1 <= 0 || tri2 <= 0 || tri3 <= 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
        }

        private void showErrorText()
        {
            MessageBox.Show("All parameters must be filled in with a positive, non-zero number.");
        }

        private void optionsButton_Click(object sender, EventArgs e)
        {
            Form2 optionsForm = new Form2();
            optionsForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 historyForm = new Form3();
            historyForm.Show();
        }

        private void getFromDatabase()
        {
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = "SELECT * FROM Calculator_Variables";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                decimals = Convert.ToInt32(reader["decimalPlaces"]);
                history = Convert.ToInt32(reader["historyEntries"]);
            }

            con.Close();
        }

        private void insertData(string query)
        {
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();

            deleteExtra(con);
            con.Close();
        }

        private void deleteExtra(SqlConnection con)
        {
            string query = "DELETE FROM CalculationHistory WHERE calcTime NOT IN " +
                           "(SELECT TOP 20 calcTime FROM CalculationHistory ORDER BY calcTime DESC)";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
        }

    }
}
