This program calculates the area, perimeter, volume, and surface area of various shapes and displays the results to the user.  The user has the option to change the maximum number of decimals in the result and can also view up to twenty entries in the calculation history.  A video demonstration is included in the file Calculator.mkv.

The password required to use the access the database has been removed, so the application will not function in this state.
